<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import = "java.io.*,java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Amatic+SC">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <style>
        body, html {height: 100%}
        body,h1,h2,h3,h4,h5,h6 {font-family: "Amatic SC", sans-serif}
        
        .adminPage{
            background-color: #F5F5DC;
        }
        </style>
        
    <body class="adminPage">

        <!-- Navbar (sit on top) -->
        <div class="w3-top w3-hide-small">
            <div class="w3-bar w3-xlarge w3-black w3-hover-opacity-off" id="myNavbar">
                <a href="homePageServlet" class="nav-link w3-bar-item w3-button w3-padding-large">Főoldal<span class="sr-only"></span></a>
                <a href="lostPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Elveszett</a>
                <a href="foundPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Megtalált</a>
                <a href="storyPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sikertörténetek</a>
                <a href="aboutUsPageServlet" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Rólunk</a>
                <c:if test="${pageContext.request.isUserInRole('user') || pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarUserPage.jsp" />
                    </div>
                </c:if>
                
                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <div id="topmenu_navbar">
                        <jsp:include page="navBarAdminPage.jsp" />
                    </div>
                </c:if>
                
                <div id="topmenu_navbar">
                    <jsp:include page="navBarLoginRegistrationPage.jsp" />
                </div>
            </div>
        </div>

        <div class="container-fluid text-center">    
            <div class="row content">
                <c:if test="${pageContext.request.isUserInRole('admin')}">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div style="background-color: rgba(0,0,0,0.6)" > 
                        <h1 style="font-size:2vw; color:white">Hello Admin!</h1>
                        <h2 style="font-size:large; color:white">Önkéntesek listája:</h2>
                        <div class="container p-3 mb-2 bg-white text-dark">
                        
                            <form action="adminPageServlet" method="post">
                                <c:if test="${not empty users}">
                                    <table class="table" style="font-size:large; vertical-align: middle; background-color: whitesmoke; color: darkgreen">
                                        <th>Becenév</th>
                                        <th>Email cim</th>
                                        <th>Vezeték név</th>
                                        <th>Kereszt név</th>
                                        <th>Beléphet?</th>
                                        <th>
                                            <input type="submit" value="save" name="Mentés" class="form-control">
                                        </th>
                                        <c:forEach var="item" items="${users}">
                                            <tr>
                                                <td>${item.nickName}</td>
                                                <td>${item.userEmail}</td>
                                                <td>${item.userLastName}</td>
                                                <td>${item.userFirstName}</td>
                                                <td>
                                                    <select name="${item.nickName}" style="color: black">
                                                        <option ${item.valid == 'true' ? 'selected' : ''} value="true">igen</option>
                                                        <option ${item.valid == 'false' ? 'selected' : ''} value="false">nem</option>
                                                    </select>
                                                </td>                                           
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </c:if>
                            </form>
                        
                        </div>                   
                    </div>
                </c:if>
            </div>        
        </div>

                <!-- Footer -->
        <footer class="w3-container w3-padding-64 w3-buttom w3-opacity w3-light-grey w3-xlarge">
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://wwf.hu/" style="color:#009900"> PetSaver</a>
            </div>
        </footer>
    </body>
</html>
