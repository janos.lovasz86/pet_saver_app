<%-- 
    Document   : imageUploadErrorPage
    Created on : 2019.11.11., 22:54:00
    Author     : Mr. Lovasz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Képfeltöltés hiba</title>
    </head>
    <body>
        <h1>Hiba a kép feltöltése során!</h1>
        
        <a href="toUploadServlet">Vissza a képfeltöltéshez!</a>
    </body>
</html>
