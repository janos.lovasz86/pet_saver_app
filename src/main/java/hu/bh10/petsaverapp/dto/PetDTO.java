package hu.bh10.petsaverapp.dto;

public class PetDTO {
    
    private Long id;
    private String nickName;   
    private boolean catOrDog;//TRUE == Kutya, FALSE == Macska
    private int age; 
    private String color;
    private boolean sex;//TRUE == Him, FALSE == Nőstény
    private double weight;
    private String type;
    private String subType;
    private boolean owned;
    private boolean dangerous;
    private String image;
    private int interestCounter;
    private boolean success;
    private boolean found;
    private UserDTO user;
    private AnnouncerDTO announcer;
    private FosterDTO foster;

    public PetDTO() {
    }

    public PetDTO(Long id, String nickName, boolean catOrDog, int age, String color, boolean sex, double weight, String type, String subType, boolean owned, boolean dangerous, String image, int interestCounter, boolean success, boolean found) {
        this.id = id;
        this.nickName = nickName;
        this.catOrDog = catOrDog;
        this.age = age;
        this.color = color;
        this.sex = sex;
        this.weight = weight;
        this.type = type;
        this.subType = subType;
        this.owned = owned;
        this.dangerous = dangerous;
        this.image = image;
        this.interestCounter = interestCounter;
        this.success = success;
        this.found = found;
    }

    public PetDTO(Long id, String nickName, boolean catOrDog, int age, String color, boolean sex, String image, boolean found) {
        this.id = id;
        this.nickName = nickName;
        this.catOrDog = catOrDog;
        this.age = age;
        this.color = color;
        this.sex = sex;
        this.image = image;
        this.found = found;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public boolean isCatOrDog() {
        return catOrDog;
    }

    public void setCatOrDog(boolean catOrDog) {
        this.catOrDog = catOrDog;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public boolean isOwned() {
        return owned;
    }

    public void setOwned(boolean owned) {
        this.owned = owned;
    }

    public boolean isDangerous() {
        return dangerous;
    }

    public void setDangerous(boolean dangerous) {
        this.dangerous = dangerous;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getInterestCounter() {
        return interestCounter;
    }

    public void setInterestCounter(int interestCounter) {
        this.interestCounter = interestCounter;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    
    
    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public AnnouncerDTO getAnnouncer() {
        return announcer;
    }

    public void setAnnouncer(AnnouncerDTO announcer) {
        this.announcer = announcer;
    }

    public FosterDTO getFoster() {
        return foster;
    }

    public void setFoster(FosterDTO foster) {
        this.foster = foster;
    }

    @Override
    public String toString() {
        return "PetDTO{" + "id=" + id + ", nickName=" + nickName + ", catOrDog=" + catOrDog + ", age=" + age + ", color=" + color + ", sex=" + sex + ", weight=" + weight + ", type=" + type + ", subType=" + subType + ", owned=" + owned + ", dangerous=" + dangerous + ", image=" + image + ", interestCounter=" + interestCounter + ", success=" + success + ", found=" + found + ", user=" + user + ", announcer=" + announcer + ", foster=" + foster + '}';
    }
    
}
