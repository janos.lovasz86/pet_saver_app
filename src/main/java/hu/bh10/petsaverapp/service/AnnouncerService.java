package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.AnnouncerDAO;
import hu.bh10.petsaverapp.dto.AnnouncerDTO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import hu.bh10.petsaverapp.mapper.AnnouncerMapper;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class AnnouncerService {

    @Inject
    AnnouncerDAO announcerDAO;

    public void createAnnouncer(AnnouncerDTO announcer) {
        if(!checkAnnouncerEmail(announcer.getEmailOfAnnouncer())){
            AnnouncerEntity entity = AnnouncerMapper.toAnnouncerEntity(announcer);
            announcerDAO.createEntity(entity);
            announcer.setAnnouncerId(entity.getId());
        }
    }

    public AnnouncerService() {
    }

    public AnnouncerService(AnnouncerDAO announcerDAO) {
        this.announcerDAO = announcerDAO;
    }
    
    
    
    public void updateAnnouncer(AnnouncerDTO announcer){
        announcerDAO.updateAnnouncer(AnnouncerMapper.toAnnouncerEntity(announcer));
    }
    
    public boolean checkAnnouncerEmail(String announcerEmail){
        return announcerDAO.checkAnnouncerEmail(announcerEmail);
    }
    
    public AnnouncerDTO getAnnouncerByEmail(String announcerEmail){
        return AnnouncerMapper.toAnnouncerDTO(announcerDAO.getAnnouncerByEmail(announcerEmail));
    }
    
    public AnnouncerDTO getAnnouncerById(Long announcerId){
        return AnnouncerMapper.toAnnouncerDTO(announcerDAO.findAnnouncerById(announcerId));
    }
    
    public AnnouncerEntity getAnnounceerById(Long announcerId){
        return announcerDAO.findAnnouncerById(announcerId);
    }
    
    public Long getAnnouncerIdByEmail(String announcerEmail){
        return announcerDAO.getAnnouncerIdByEmail(announcerEmail);
    }

    public List<AnnouncerEntity> findAll() {
        return announcerDAO.findAll(AnnouncerEntity.class);
    }

    public void removeAnnouncer(AnnouncerEntity announcerEntity, Long id) {
        announcerDAO.removeAnnouncer(announcerEntity, id);
    }

}
