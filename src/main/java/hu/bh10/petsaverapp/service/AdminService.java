package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.AdminDAO;
import hu.bh10.petsaverapp.dto.UserDTO;
import hu.bh10.petsaverapp.dto.UserGroupDTO;
import hu.bh10.petsaverapp.entity.P_UserEntity;
import hu.bh10.petsaverapp.entity.UserEntity;
import hu.bh10.petsaverapp.entity.UserGroup;
import hu.bh10.petsaverapp.mapper.UserGroupMapper;
import hu.bh10.petsaverapp.mapper.UserMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class AdminService {
    
    @Inject
    AdminDAO adminDAO;
    
     public void updateUsers(List<UserDTO> usersDTO){
         List<UserEntity> users = new ArrayList<>();
         List<P_UserEntity> p_users = adminDAO.getPUsers();
         for (int i = 0; i < usersDTO.size(); i++) {
             
            String nickName = usersDTO.get(i).getNickName();
            String role = usersDTO.get(i).getRole();
            String userEmail = usersDTO.get(i).getUserEmail();
            String userFirstName = usersDTO.get(i).getUserFirstName();
            String userLastName = usersDTO.get(i).getUserLastName();
            String userPassword = usersDTO.get(i).getUserPassword();
            boolean valid = usersDTO.get(i).isValid();
            
            UserEntity entity = UserMapper.toEntity(nickName, role, userEmail, userFirstName, userLastName, userPassword, valid);
            users.add(entity);
            
            
            //System.out.println("Entitás: "+entity);
         }
        try {
            adminDAO.setUsers(users, p_users); // saveUsers
        } catch (Exception ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<UserDTO> getUserList(){
        List<UserDTO> users = new ArrayList<>();
        users = UserMapper.toUserDTOList(adminDAO.getUsers());
        return users;
    }
    
    public List<UserGroupDTO> getGroupList(List<UserDTO> usersDTO){
        List<UserGroup> groups = new ArrayList<>();
        for (UserDTO user : usersDTO) {
            //egy email alapján visszakapunk egy UserGroupEntity listát, amit átalakitunk String listává
            groups.addAll(adminDAO.getGroups(user.getUserEmail()));
        }
        List<UserGroupDTO> groupDTOList = UserGroupMapper.toUserGroupDTOList(groups);
        return groupDTOList;
    }
    
}
