package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.LostDTO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import hu.bh10.petsaverapp.entity.LostEntity;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class LostMapper {

    public LostMapper() {}
   
    public static LostEntity toLostEntity(LostDTO lost, AnnouncerEntity entity) {
        LostEntity lostEntity = new LostEntity();
//        lostEntity.setId(lost.getId());
        lostEntity.setNickName(lost.getNickName());
        lostEntity.setLastKnownPlace(lost.getLastKnownPlace());
        lostEntity.setSex(lost.getSex());
        lostEntity.setPetProperties(lost.getPetProperties());
        LocalDate announceDate = lost.getLostDate();
        Date lostDate = Date.valueOf(announceDate);
        lostEntity.setLostDate(lostDate);
        lostEntity.setLostAnnouncer(entity);
        lostEntity.setImage(lost.getImage());
        if(lost.isAdded()){
           lostEntity.setAdded("true"); 
        }else{
           lostEntity.setAdded("false");
        }

        return lostEntity;
    }

    public static LostDTO toLostDTO(LostEntity lostEntity) {

        LostDTO dto = new LostDTO();
        dto.setId(lostEntity.getId());
        dto.setNickName(lostEntity.getNickName());
        dto.setLastKnownPlace(lostEntity.getLastKnownPlace());
        dto.setSex(lostEntity.getSex());
        dto.setPetProperties(lostEntity.getPetProperties());
        LocalDate gotDate = lostEntity.getLostDate().toLocalDate();
        dto.setLostDate(gotDate);
        dto.setImage(lostEntity.getImage());
        if(lostEntity.getAdded().equals("true")){
            dto.setAdded(true);
        }else{
            dto.setAdded(false);
        }
        dto.setAnnouncer(AnnouncerMapper.toAnnouncerDTO(lostEntity.getLostAnnouncer()));

        return dto;
    }

    public static List<LostDTO> toLostDTO(List<LostEntity> lostEntities) {
        List<LostDTO> lostDTOList = new ArrayList<>();
//        lostEntities.forEach(lost -> lostDTOList.add(toLostDTO(lost)));
        for (int i = 0; i < lostEntities.size(); i++) {
            LostDTO lostDTO = new LostDTO();
            lostDTO.setLastKnownPlace(lostEntities.get(i).getLastKnownPlace());
            lostDTO.setSex(lostEntities.get(i).getSex());
            lostDTO.setPetProperties(lostEntities.get(i).getPetProperties());
            LocalDate gotDate = lostEntities.get(i).getLostDate().toLocalDate();
            lostDTO.setLostDate(gotDate);
            lostDTO.setImage(lostEntities.get(i).getImage());
            lostDTOList.add(lostDTO);
        }
        return lostDTOList;

    }
}
