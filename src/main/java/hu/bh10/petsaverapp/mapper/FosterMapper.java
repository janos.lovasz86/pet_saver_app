package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.FosterDTO;
import hu.bh10.petsaverapp.entity.FosterEntity;
import java.sql.Date;
import java.time.LocalDate;

public class FosterMapper {

    public FosterMapper() {
    }
    
    public static FosterEntity toEntity(FosterDTO dto) {
        FosterEntity entity = new FosterEntity();
        entity.setId(dto.getId());
        entity.setFosterEmail(dto.getFosterEmail());
        entity.setFosterPhone(dto.getFosterPhone());
        entity.setFosterFirstName(dto.getFosterFirstName());
        entity.setFosterLastName(dto.getFosterLastName());
//        Date fosterDate = Date.valueOf(dto.getFosterDate());
//        entity.setFosterDate(fosterDate);
        entity.setFosterNote(dto.getFosterNote());


        return entity;
    }

    public static FosterDTO toDTO(FosterEntity entity) {

        FosterDTO dto = new FosterDTO();
        dto.setId(entity.getId());
        dto.setFosterEmail(entity.getFosterEmail());
        dto.setFosterPhone(entity.getFosterPhone());
        dto.setFosterFirstName(entity.getFosterFirstName());
        dto.setFosterLastName(entity.getFosterLastName());
//        LocalDate dateFromEntity = entity.getFosterDate().toLocalDate();
//        dto.setFosterDate(dateFromEntity);
        dto.setFosterNote(entity.getFosterNote());

        return dto;
    }
    
}
