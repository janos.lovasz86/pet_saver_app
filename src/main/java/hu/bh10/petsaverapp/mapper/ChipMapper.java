package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.ChipDTO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.entity.ChipEntity;
import hu.bh10.petsaverapp.entity.PetEntity;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ChipMapper {
    
    public ChipMapper() {
    }
   
    public static ChipEntity toEntity(ChipDTO dto, PetEntity pet) {
        ChipEntity entity = new ChipEntity();

        entity.setChipNumber(dto.getChipNumber());
        Date chipDate = Date.valueOf(dto.getChipDate());
        entity.setChipDate(chipDate);
        entity.setVetName(dto.getVetName());
        entity.setChipedPet(pet);

        return entity;
    }

    public static ChipDTO toDTO(ChipEntity entity, PetDTO petDTO) {

        ChipDTO dto = new ChipDTO();
        dto.setId(entity.getId());
        dto.setChipNumber(entity.getChipNumber());
        LocalDate dateFromEntity = entity.getChipDate().toLocalDate();
        dto.setChipDate(dateFromEntity);
        dto.setVetName(entity.getVetName());
        dto.setPet(petDTO);

        return dto;
    }
    
    public static ChipEntity toChipEntity(ChipDTO dto) {
        ChipEntity entity = new ChipEntity();

        entity.setChipNumber(dto.getChipNumber());
        Date chipDate = Date.valueOf(dto.getChipDate());
        entity.setChipDate(chipDate);
        entity.setVetName(dto.getVetName());
        entity.setChipedPet(PetMapper.toPetEntity(dto.getPet()));

        return entity;
    }
    
    public static ChipDTO toChipDTO(ChipEntity entity) {

        ChipDTO dto = new ChipDTO();
        dto.setId(entity.getId());
        dto.setChipNumber(entity.getChipNumber());
        LocalDate dateFromEntity = entity.getChipDate().toLocalDate();
        dto.setChipDate(dateFromEntity);
        dto.setVetName(entity.getVetName());
        dto.setPet(PetMapper.toPetDTO(entity.getChipedPet()));

        return dto;
    }
    
    public static List<ChipDTO> toChipDTOList(List<ChipEntity> entities){
        List<ChipDTO> dtos = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            ChipDTO chip = ChipMapper.toChipDTO(entities.get(i));
            dtos.add(chip);
        }
        return dtos;
    }
    
    public static List<ChipEntity> toChipEntityList(List<ChipDTO> dtos){
        List<ChipEntity> entities = new ArrayList<>();
        for (int i = 0; i < dtos.size(); i++) {
            ChipEntity entity = ChipMapper.toChipEntity(dtos.get(i));
            entities.add(entity);
        }
        return entities;
    }
}
