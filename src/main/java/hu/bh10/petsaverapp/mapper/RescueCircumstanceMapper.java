package hu.bh10.petsaverapp.mapper;

import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.dto.RescueCircumstanceDTO;
import hu.bh10.petsaverapp.entity.PetEntity;
import hu.bh10.petsaverapp.entity.RescueCircumstanceEntity;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RescueCircumstanceMapper {
    
    public RescueCircumstanceMapper() {
    }
   
    public static RescueCircumstanceEntity toEntity(RescueCircumstanceDTO dto, PetEntity pet) {
        RescueCircumstanceEntity entity = new RescueCircumstanceEntity();
        
        Date rescueDate = Date.valueOf(dto.getRescueDate());
        entity.setRescueDate(rescueDate);
        entity.setRescueTime(dto.getRescueTime());
        entity.setRescuePlace(dto.getRescuePlace());
        entity.setRescueNote(dto.getRescueNote());
        entity.setRescuedPet(pet);

        return entity;
    }

    public static RescueCircumstanceDTO toDTO(RescueCircumstanceEntity entity, PetDTO petDTO) {
        RescueCircumstanceDTO dto = new RescueCircumstanceDTO();
        
        LocalDate dateFromEntity = entity.getRescueDate().toLocalDate();
        dto.setRescueDate(dateFromEntity);
        dto.setRescueTime(entity.getRescueTime());
        dto.setRescuePlace(entity.getRescuePlace());
        dto.setRescueNote(entity.getRescueNote());
        dto.setPet(petDTO);

        return dto;
    }
    
    public static RescueCircumstanceEntity toRescueEntity(RescueCircumstanceDTO dto){
        RescueCircumstanceEntity entity = new RescueCircumstanceEntity();
        
        Date rescueDate = Date.valueOf(dto.getRescueDate());
        entity.setRescueDate(rescueDate);
        entity.setRescueTime(dto.getRescueTime());
        entity.setRescuePlace(dto.getRescuePlace());
        entity.setRescueNote(dto.getRescueNote());
        entity.setRescuedPet(PetMapper.toPetEntity(dto.getPet()));
        
        return entity;
    }
    
    public static RescueCircumstanceDTO toRescueDTO(RescueCircumstanceEntity entity) {
        RescueCircumstanceDTO dto = new RescueCircumstanceDTO();
        
        dto.setId(entity.getId());
        LocalDate dateFromEntity = entity.getRescueDate().toLocalDate();
        dto.setRescueDate(dateFromEntity);
        dto.setRescueTime(entity.getRescueTime());
        dto.setRescuePlace(entity.getRescuePlace());
        dto.setRescueNote(entity.getRescueNote());
        dto.setPet(PetMapper.toPetDTO(entity.getRescuedPet()));

        return dto;
    }
    
    public static List<RescueCircumstanceDTO> toRescueDTOList(List<RescueCircumstanceEntity> entities){
        List<RescueCircumstanceDTO> dtos = new ArrayList<>();
        for (int i = 0; i < entities.size(); i++) {
            RescueCircumstanceDTO dto = RescueCircumstanceMapper.toRescueDTO(entities.get(i));
            dtos.add(dto);
        }
        return dtos;
    }
    
    public static List<RescueCircumstanceEntity> toRescueEntityList(List<RescueCircumstanceDTO> dtos){
        List<RescueCircumstanceEntity> entities = new ArrayList<>();
        for (int i = 0; i < dtos.size(); i++) {
            RescueCircumstanceEntity entity = RescueCircumstanceMapper.toRescueEntity(dtos.get(i));
            entities.add(entity);
        }
        return entities;
    }
}
