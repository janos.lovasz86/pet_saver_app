package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.FosterEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

@Stateless
public class FosterDAO extends BaseDAO{


    @Transactional
    public void createEntity(FosterEntity fosterEntity){
        em.persist(fosterEntity);
        em.flush();
        
    }
    
    public FosterEntity getFosterByEmail(String email) {
        return em.createNamedQuery(FosterEntity.NQ_GET_FOSTER_BY_EMAIL, FosterEntity.class)
                .setParameter(FosterEntity.PARAM_FOSTER_EMAIL, email)
                .getSingleResult();
    }
    
//    public boolean fosterIsExist(String email) {
//        System.out.println("Idáig eljutunk?");
//        int emailNumber = 0;
//        emailNumber = ((Integer) em.createNamedQuery(FosterEntity.NQ_CHECK_FOSTER_BY_EMAIL, Integer.class)
//                .setParameter(FosterEntity.PARAM_FOSTER_EMAIL, email)
//                .getSingleResult().intValue());
//        System.out.println("Email number: "+emailNumber);
//        return emailNumber > 0;
//    }
    
    public boolean fosterIsExist(String email) {
        List list = em.createNamedQuery(FosterEntity.NQ_CHECK_FOSTER_BY_EMAIL, FosterEntity.class)
                .setParameter(FosterEntity.PARAM_FOSTER_EMAIL, email)
                .getResultList();
        if(list.isEmpty()){      
            return false;
        }else{
            return true;
        }
    }
}
