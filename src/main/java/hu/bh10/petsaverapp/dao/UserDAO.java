package hu.bh10.petsaverapp.dao;

import hu.bh10.petsaverapp.entity.BaseEntity;
import hu.bh10.petsaverapp.entity.UserEntity;
import static hu.bh10.petsaverapp.entity.UserEntity.NQ_GET_USERS;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Stateless
public class UserDAO{
    
    @PersistenceContext(unitName = "PetSaverAppPU")
    protected EntityManager em;
    
    
    @Transactional
    public void createEntity(UserEntity entity){
        System.out.println("Entity a persist elott: "+entity);
        em.persist(entity);
    }
    
    public List<UserEntity> getUsers(){
        List<UserEntity> userList = new ArrayList();
        userList =  em.createNamedQuery(UserEntity.NQ_GET_USERS, UserEntity.class)
                .getResultList();
        return userList;
    }
    
    public UserEntity getUserByEmail(String email) {
        return em.createNamedQuery(UserEntity.NQ_GET_USER_BY_EMAIL, UserEntity.class)
                .setParameter(UserEntity.PARAM_USER_EMAIL, email)
                .getSingleResult();
    }
    // DAO -> menjenek adatbázisművelet, felesleges logika
    public boolean authenticate(String email,String password) throws Exception {
        boolean authenticated=false;
        System.out.println("A Payara használja itt ezt az Authentikációt?");//NEM!!!
        List<UserEntity> userList = getUsers();
        for (UserEntity user : userList) {
            if(user.getUserEmail().equals(email)){
                if(user.getUserPassword().equals(password)){
                    if(user.getValid().equals("yes")){
                        authenticated=true;
                    }           
                }
            }
        }		
        return authenticated;		
    }

    public boolean isExist(String email) throws Exception {
        boolean exist=false;
        List<UserEntity> userList = getUsers();
        for (UserEntity user : userList) {
            if(user.getUserEmail().equals(email)){
                exist = true;
            }
        }
        return exist;		
    }
}
