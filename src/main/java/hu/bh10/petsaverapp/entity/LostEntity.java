package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.FoundEntity.PARAM_ADDED;
import static hu.bh10.petsaverapp.entity.LostEntity.NQ_GET_LOST_BUT_NOT_ADDED_PET;
import static hu.bh10.petsaverapp.entity.LostEntity.NQ_GET_LOST_BY_ID;
import static hu.bh10.petsaverapp.entity.LostEntity.PARAM_ID;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "lost_pet")
@NamedQueries({
    @NamedQuery(name = NQ_GET_LOST_BUT_NOT_ADDED_PET, query = "select l from LostEntity l where l.added =:"+ PARAM_ADDED),
    @NamedQuery(name = NQ_GET_LOST_BY_ID, query = "select l from LostEntity l where l.id =:"+ PARAM_ID)
})
public class LostEntity extends BaseEntity{
    
    public static final String NQ_GET_LOST_BUT_NOT_ADDED_PET = "FoundEntity.getLostButNotAddedList";
    public static final String NQ_GET_LOST_BY_ID = "LostEntity.getLostById";
    public static final String PARAM_ADDED = "added";
    public static final String PARAM_ID = "id";
    
    @Column(name = "nick_name", nullable = false)
    private String nickName;
    
    @Column(name = "last_known_place", nullable = false)
    private String lastKnownPlace;
    
    @Column
    private String sex;
    
    @Column(name = "pet_properties", nullable = false)
    private String petProperties;
    
    @Column(name = "lost_Date", nullable = false)
    private Date lostDate;
    
    @JoinColumn(name = "announcer_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private AnnouncerEntity lostAnnouncer;
    
    @Column
    private String image;
    
    @Column
    private String added;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getLastKnownPlace() {
        return lastKnownPlace;
    }

    public void setLastKnownPlace(String lastKnownPlace) {
        this.lastKnownPlace = lastKnownPlace;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPetProperties() {
        return petProperties;
    }

    public void setPetProperties(String petProperties) {
        this.petProperties = petProperties;
    }

    public Date getLostDate() {
        return lostDate;
    }

    public void setLostDate(Date lostDate) {
        this.lostDate = lostDate;
    }

    public AnnouncerEntity getLostAnnouncer() {
        return lostAnnouncer;
    }

    public void setLostAnnouncer(AnnouncerEntity lostAnnouncer) {
        this.lostAnnouncer = lostAnnouncer;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }
    
    
}
