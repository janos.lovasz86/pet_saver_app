package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.VaccinationEntity.NQ_GET_VACCINATIONS_BY_ID;
import static hu.bh10.petsaverapp.entity.VaccinationEntity.PARAM_PET_ID;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "vaccinaction")
@NamedQueries({
    @NamedQuery(name = NQ_GET_VACCINATIONS_BY_ID, query = "select v from VaccinationEntity v where v.vaccinatedPet.id =:"+ PARAM_PET_ID)
})
public class VaccinationEntity extends BaseEntity{
    
    public static final String NQ_GET_VACCINATIONS_BY_ID = "VaccinationEntity.getVaccinations";
    public static final String PARAM_PET_ID = "petId";
    
    @Column(name = "vacc_date", nullable = false)
    private Date vaccDate;
    
    @Column(name = "vacc_type", nullable = false)
    private String vaccType;
    
    @JoinColumn(name = "pet_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private PetEntity vaccinatedPet;

    public Date getVaccDate() {
        return vaccDate;
    }

    public void setVaccDate(Date vaccDate) {
        this.vaccDate = vaccDate;
    }

    public String getVaccType() {
        return vaccType;
    }

    public void setVaccType(String vaccType) {
        this.vaccType = vaccType;
    }

    public PetEntity getVaccinatedPet() {
        return vaccinatedPet;
    }

    public void setVaccinatedPet(PetEntity vaccinatedPet) {
        this.vaccinatedPet = vaccinatedPet;
    }
    
    
}
