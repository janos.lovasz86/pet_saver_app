package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.FoundEntity.NQ_GET_FOUND_BUT_NOT_ADDED_PET;
import static hu.bh10.petsaverapp.entity.FoundEntity.NQ_GET_FOUND_BY_ID;
import static hu.bh10.petsaverapp.entity.FoundEntity.PARAM_ADDED;
import static hu.bh10.petsaverapp.entity.FoundEntity.PARAM_ID;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "found_pet")
@NamedQueries({
    @NamedQuery(name = NQ_GET_FOUND_BUT_NOT_ADDED_PET, query = "select f from FoundEntity f where f.added =:"+ PARAM_ADDED),
    @NamedQuery(name = NQ_GET_FOUND_BY_ID, query = "select f from FoundEntity f where f.id =:"+ PARAM_ID)
})
public class FoundEntity extends BaseEntity{
    
    public static final String NQ_GET_FOUND_BUT_NOT_ADDED_PET = "FoundEntity.getFoundButNotAddedList";
    public static final String NQ_GET_FOUND_BY_ID = "FoundEntity.getFoundById";
    public static final String PARAM_ADDED = "added";
    public static final String PARAM_ID = "id";
    
    @Column(name = "found_place", nullable = false)
    private String foundPlace;
    
    @Column
    private String sex;
    
    @Column(name = "pet_properties", nullable = false)
    private String petProperties;
    
    @Column(name = "found_date", nullable = false)
    private Date foundDate;
    
    @JoinColumn(name = "announcer_id")
    @ManyToOne(fetch = FetchType.EAGER )
    private AnnouncerEntity foundAnnouncer;
    
    @Column
    private String image;
    
    @Column
    private String added;

    public String getFoundPlace() {
        return foundPlace;
    }

    public void setFoundPlace(String foundPlace) {
        this.foundPlace = foundPlace;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPetProperties() {
        return petProperties;
    }

    public void setPetProperties(String petProperties) {
        this.petProperties = petProperties;
    }

    public Date getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(Date foundDate) {
        this.foundDate = foundDate;
    }

    public AnnouncerEntity getFoundAnnouncer() {
        return foundAnnouncer;
    }

    public void setFoundAnnouncer(AnnouncerEntity foundAnnouncer) {
        this.foundAnnouncer = foundAnnouncer;
    }
    

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }
    
    
}
