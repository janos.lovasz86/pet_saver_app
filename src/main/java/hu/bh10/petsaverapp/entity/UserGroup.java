package hu.bh10.petsaverapp.entity;

import static hu.bh10.petsaverapp.entity.UserGroup.NQ_GET_USER_GROUPS_BY_EMAIL;
import static hu.bh10.petsaverapp.entity.UserGroup.PARAM_USER_EMAIL;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user_group")
@NamedQuery(name = NQ_GET_USER_GROUPS_BY_EMAIL, query = "select ug.groupName from UserGroup ug join ug.userGroups userG where userG.userEmail = :"+PARAM_USER_EMAIL)
public class UserGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String NQ_GET_USER_GROUPS_BY_EMAIL = "UserGroup.getUserGroupsByEmail";
    public static final String PARAM_USER_EMAIL = "email";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "group_name", unique = true)
    private String groupName;

    @ManyToMany 
    @JoinTable(name = "user_user_group",
            joinColumns = @JoinColumn(name = "group_name", referencedColumnName = "group_name"),
            inverseJoinColumns = @JoinColumn(name = "user_email", referencedColumnName = "user_email"))
    private List<UserEntity> userGroups;
    
//    @OneToMany(mappedBy = "userRole")
//    private List<UserEntity> users = new ArrayList<UserEntity>();

    public UserGroup() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<UserEntity> getUsers() {
        return userGroups;
    }

    public void setUsers(List<UserEntity> userGroups) {
        this.userGroups = userGroups;
    }

    public List<UserEntity> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<UserEntity> userGroups) {
        this.userGroups = userGroups;
    }
    

}
