package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.FosterDTO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.mapper.PetMapper;
import hu.bh10.petsaverapp.service.FosterService;
import hu.bh10.petsaverapp.service.PetService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "FosterRegistrationServlet", urlPatterns = {"/fosterRegistrationServlet"})
public class FosterRegistrationServlet extends HttpServlet {

    @Inject
    FosterService fosterService;
    
    @Inject
    PetService petService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //El kell kérni a foundPage.jsp-től a petId-t
        HttpSession session = request.getSession();
        Long petId = Long.valueOf(request.getParameter("petId"));
        System.out.println("Kiallat ID: "+petId);
        PetDTO pet = PetMapper.toPetDTO(petService.getPetById(petId));
        session.setAttribute("pet", pet);
        
        request.getRequestDispatcher("WEB-INF/fosterRegistration.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        
        PetDTO pet = (PetDTO) session.getAttribute("pet");
        Long petId = pet.getId();
        
        String email = request.getParameter("email");
        System.out.println("Foster email: "+email);
        String phone = request.getParameter("phone");
        String lastName = request.getParameter("lastName");
        String firstName = request.getParameter("firstName");

        FosterDTO freshFoster = new FosterDTO();
        if(!fosterService.fosterIsExist(email)){
            freshFoster.setFosterFirstName(firstName);
            freshFoster.setFosterLastName(lastName);
            freshFoster.setFosterEmail(email);
            freshFoster.setFosterPhone(phone);
            fosterService.createFoster(freshFoster);
        }      
        FosterDTO savedFoster = fosterService.getFosterByEmail(email);
        System.out.println("Foster: "+savedFoster);

        
        petService.updatePetToFosteredPet(petId, savedFoster);

        request.getRequestDispatcher("WEB-INF/successfullFosterRegistration.jsp").forward(request, response);
//        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
