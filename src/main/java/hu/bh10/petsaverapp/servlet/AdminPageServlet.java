package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.UserDTO;
import hu.bh10.petsaverapp.service.AdminService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "adminPageServlet", urlPatterns = {"/adminPageServlet"})
@DeclareRoles({"admin"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin"}))
public class AdminPageServlet extends HttpServlet {
    
    @Inject
    AdminService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<UserDTO> users = service.getUserList();

        HttpSession session = request.getSession();
        session.setAttribute("users", users);

        request.getRequestDispatcher("WEB-INF/adminPage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        List<UserDTO> userDTOList = (ArrayList<UserDTO>) session.getAttribute("users");
        for (UserDTO user : userDTOList) {
            if(request.getParameter(user.getNickName()).equals("true")){
                user.setValid(true);
            }else{
                user.setValid(false);
            }           
        }
        service.updateUsers(userDTOList);

//        response.sendRedirect("homePageServlet");
        request.getRequestDispatcher("WEB-INF/adminPage.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}




////        List<UserDTO> userDTOList = ((List<UserDTO>) request.getAttribute("users"));
////        List<UserDTO> userDTOList = (ArrayList<UserDTO>) request.getAttribute("users");
////        System.out.println("userDTOList: "+userDTOList);
//        List<UserDTO> userList = new ArrayList<>();
//        for (int i = 0; i < userDTOList.size(); i++) {
//            UserDTO user = new UserDTO();
//            user.setNickName(userDTOList.get(i).getNickName());
//            user.setUserEmail(userDTOList.get(i).getUserEmail());
//            user.setUserLastName(userDTOList.get(i).getUserLastName());
//            user.setUserFirstName(userDTOList.get(i).getUserFirstName());
//            user.setUserPassword(userDTOList.get(i).getUserPassword());
//            user.setRole(userDTOList.get(i).getRole());
////            String roleOfUser = (String)request.getParameter("role");
////            if(roleOfUser.equals("admin")){
////                user.setRole("admin");
////            }else{
////                user.setRole("user");
////            }  
//            user.setValid(userDTOList.get(i).isValid());
////            String validUser = (String)request.getParameter("valid");
////            if(validUser.equals("igen")){
////                user.setValid(true);
////            }else{
////                user.setValid(false);
////            }
//            userList.add(user);
//        }