package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.FosterDTO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.dto.RescueCircumstanceDTO;
import hu.bh10.petsaverapp.mapper.PetMapper;
import hu.bh10.petsaverapp.service.FosterService;
import hu.bh10.petsaverapp.service.PetService;
import hu.bh10.petsaverapp.service.RescueCircumstanceService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "UnSuccessAdoptionServlet", urlPatterns = {"/unSuccessAdoptionServlet"})
@DeclareRoles({"admin", "user"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user"}))
public class UnSuccessAdoptionServlet extends HttpServlet {

    @Inject
    private PetService petService;
    
    @Inject
    private RescueCircumstanceService rescueService;
    
    @Inject
    private FosterService fosterService;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
//        HttpSession session = request.getSession();
        Long petId = Long.valueOf(request.getParameter("petId"));
        
        //a pet DB-be átállitódik az interest_counter 0-ra, és a foster_id NULL-ra.
        petService.updatePetToUnFosteredPet(petId);
        
        response.sendRedirect("userPageServlet");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
