package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.dto.FosterDTO;
import hu.bh10.petsaverapp.dto.PetDTO;
import hu.bh10.petsaverapp.dto.RescueCircumstanceDTO;
import hu.bh10.petsaverapp.mapper.PetMapper;
import hu.bh10.petsaverapp.service.FosterService;
import hu.bh10.petsaverapp.service.PetService;
import hu.bh10.petsaverapp.service.RescueCircumstanceService;
import java.io.IOException;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "SuccessAdoptionServlet", urlPatterns = {"/successAdoptionServlet"})
@DeclareRoles({"admin", "user"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user"}))
public class SuccessAdoptionServlet extends HttpServlet {
    
    @Inject
    private PetService petService;
    
    @Inject
    private RescueCircumstanceService rescueService;
    
    @Inject
    private FosterService fosterService;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //itt ráirányitjuk az önkéntest egy olyan jsp-re, amelyiken meg tudja adni a
        //fosterNote-ot, be tudja állitani a fosterDate-et.       
        HttpSession session = request.getSession();
        Long petId = Long.valueOf(request.getParameter("petId"));
        PetDTO pet = PetMapper.toPetDTO(petService.getPetById(petId));

        List<RescueCircumstanceDTO> rescues = rescueService.getRescuesById(petId);
        session.setAttribute("rescues", rescues);
        session.setAttribute("pet", pet);
        
        if(pet.isFound()){
            FosterDTO foster = fosterService.getFosterByEmail(pet.getFoster().getFosterEmail());
            session.setAttribute("foster", foster);
            request.getRequestDispatcher("WEB-INF/successAdoptionEditInfo.jsp").forward(request, response);
        }else{
            request.getRequestDispatcher("WEB-INF/successFoundEditInfo.jsp").forward(request, response);
        }       
        
        
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        Long petId = Long.valueOf(request.getParameter("petId"));
        
        //a pet DB-be átállitódik a success == true-ra.
        petService.updatePetToAdoptedPet(petId);
        
        response.sendRedirect("userPageServlet");
        
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
