package hu.bh10.petsaverapp.servlet;

import hu.bh10.petsaverapp.service.LostFormService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.annotation.security.DeclareRoles;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DeleteLostFormPetServlet", urlPatterns = {"/deleteLostFormPetServlet"})
@DeclareRoles({"admin", "user"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user"}))
public class DeleteLostFormPetServlet extends HttpServlet {

    @Inject
    private LostFormService lostFormService;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Long lostId = Long.valueOf(request.getParameter("lostId"));
        lostFormService.removeLost(lostId);
        response.sendRedirect("userPageServlet");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("userPageServlet");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
