package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.AnnouncerDAO;
import hu.bh10.petsaverapp.dao.FoundDAO;
import hu.bh10.petsaverapp.dto.AnnouncerDTO;
import hu.bh10.petsaverapp.dto.FoundDTO;
import hu.bh10.petsaverapp.entity.FoundEntity;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FoundFormServiceTest {

    private FoundFormService foundFormService;

    @Mock
    FoundDAO foundDAO;

    @Mock
    AnnouncerDAO announcerDAO;

    @Before
    public void init() {
        foundFormService = new FoundFormService(foundDAO, announcerDAO);
    }

    @Test
    public void testCreateFound() throws Exception {
        AnnouncerDTO announcerDTO = new AnnouncerDTO(2L, "Laci", "0630123456789", "email3@gmail.com");
        FoundDTO foundDTO = new FoundDTO("Fő utca 3.", "Szuka", "Sárga nyakörv", LocalDate.of(2019, 10, 22), "img", announcerDTO, false);

        foundFormService.createFound(foundDTO, Long.MIN_VALUE);

        Assert.assertEquals("Fő utca 3.", foundDTO.getFoundPlace());
        Assert.assertEquals("Szuka", foundDTO.getSex());
        Assert.assertEquals("Sárga nyakörv", foundDTO.getPetProperties());
        Assert.assertEquals(LocalDate.of(2019, 10, 22), foundDTO.getFoundDate());
        Assert.assertEquals("img", foundDTO.getImage());
        Assert.assertEquals(announcerDTO, foundDTO.getAnnouncer());
    }

    @Test
    public void testFindAll() throws Exception {
        List<FoundEntity> list = new ArrayList<>();

        FoundEntity found0 = new FoundEntity();
        FoundEntity found1 = new FoundEntity();

        list.add(found0);
        list.add(found1);

        when(foundDAO.findAll(FoundEntity.class)).thenReturn(list);

        List<FoundEntity> foundList = foundFormService.findAll();
        assertEquals(2, foundList.size());
        verify(foundDAO, times(1)).findAll(FoundEntity.class);
    }

}
