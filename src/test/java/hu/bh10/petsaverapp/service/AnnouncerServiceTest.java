package hu.bh10.petsaverapp.service;

import hu.bh10.petsaverapp.dao.AnnouncerDAO;
import hu.bh10.petsaverapp.entity.AnnouncerEntity;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class AnnouncerServiceTest {
    
    private AnnouncerService announcerService;
    
    @Mock
    AnnouncerDAO announcerDAO;
  
    @Before
    public void setUp() {
        announcerService = new AnnouncerService(announcerDAO);
    }

    @Test
    public void testFindAll() throws Exception {
        List<AnnouncerEntity> list = new ArrayList<>();
        
        AnnouncerEntity ae1 = new AnnouncerEntity("Varga Béla", "063012654", "bela@gmail.com");
        AnnouncerEntity ae2 = new AnnouncerEntity("Kiss Béla", "063032654", "kiss1@gmail.com");
        
        list.add(ae1);
        list.add(ae2);
        
        when(announcerDAO.findAll(AnnouncerEntity.class)).thenReturn(list);
        
        List<AnnouncerEntity> announcerList = announcerService.findAll();
        assertEquals(2, announcerList.size());
        verify(announcerDAO, times(1)).findAll(AnnouncerEntity.class);
    }
   
}
